package common;


public class MsgService extends Observable {

	private CommChannel channel;
	private String port;
	private int rate;
	
	public MsgService(String port, int rate){
		this.port = port;
		this.rate = rate;
	}
	
	void init(){
		try {	
			channel = new SerialCommChannel(port, rate);	
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		new Thread(() -> {
			while (true) {
				try {
					String msg = channel.receiveMsg();
					this.notifyEvent(new MsgEvent(msg));
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}).start();
	}
	
	public void sendMsg(String msg) {
		channel.sendMsg(msg);
	}
	
}
