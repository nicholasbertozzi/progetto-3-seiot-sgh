package common;

import gui.MyPanel;

//import java.io.IOException;
//import seiot.modulo_lab_3_2.common.*;
//import seiot.modulo_lab_3_2.devices.*;

public class MyAgent extends BasicEventLoopController {
	
	private MsgService msgService;
	private MyPanel panel;

	public MyAgent(String port, int rate, MyPanel myPanel){
		this.msgService = new MsgService(port, rate);		
		msgService.init();
		this.panel = myPanel;
		msgService.addObserver(this);
	}
	
	protected void processEvent(Event ev){
	    try {
	        if (ev instanceof MsgEvent){
	            String msg = ((MsgEvent) ev).getMsg();
	            System.out.println("Received: "+msg);
	            panel.getTextArea().append(msg);
		}
	    } catch (Exception ex){
	        ex.printStackTrace();
	    }
	}
	
	public MsgService getMsgService() {
	    return this.msgService;
	}
}
