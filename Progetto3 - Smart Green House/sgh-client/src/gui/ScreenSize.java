package gui;

import java.awt.Dimension;
import java.awt.Toolkit;

public class ScreenSize {
        
        public ScreenSize() {
                
        }
        
        public Dimension setDimension() {
                Toolkit t = Toolkit.getDefaultToolkit();
                Dimension sc = t.getScreenSize();
                sc.setSize((int) sc.getWidth() / 3, (int) sc.getHeight() / 5 * 4);
                return sc;
        }
}
