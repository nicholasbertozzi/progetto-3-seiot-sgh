package gui;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import common.MyAgent;
import io.vertx.core.Vertx;
import jssc.SerialPortList;

public class MyPanel extends JPanel {

        /**
         * 
         */
        private static final long serialVersionUID = 1L;
        
        private static MyFrame myframe;
        private JButton buttonStart;
        private JScrollPane scrollPanel;
        private JTextArea textArea;
        private JTextField textNgrok;
        private JComboBox<String> comboBox;
        private JLabel serialPortLable, ngrokLabel;
        private MyAgent myAgent;
        
        
        public MyPanel(MyFrame frame) {

            myframe = frame;
            this.setSize((int) (myframe.getWidth() * 0.9), (int) (myframe.getHeight() * 0.9));
            
            textArea = new JTextArea();
            buttonStart = new JButton("Comunication - START");
            scrollPanel = new JScrollPane(textArea);
            serialPortLable = new JLabel("Serial port: ");
            ngrokLabel = new JLabel("Ngrok site: ");
            textNgrok = new JTextField();
            comboBox = new JComboBox<String>();
            
            manageGraphicAppearence();
            addElementToPanel();
    }



    private void check(String[] args) throws NumberFormatException, Exception {
            GuiMain.comPortName = args[0];
            GuiMain.dataRate = Integer.parseInt(args[1]);
            textArea.setText("Start comunication with port " + GuiMain.comPortName + " at rate " + GuiMain.dataRate + "\n");
            this.myAgent = new MyAgent(GuiMain.comPortName, GuiMain.dataRate, this);
            this.myAgent.start();
            
            /* vertx magic */
            Vertx vertx = Vertx.vertx();
            vertx.setPeriodic(6010, (connect) -> {
                vertx.createHttpClient().getNow(80, this.textNgrok.getText(), "/api/data", response -> {
                    response.bodyHandler(body -> {
                        int humidity = body.toJsonObject().getInteger("humidityPercentage");
                        this.myAgent.getMsgService().sendMsg(Integer.toString(humidity));
                        System.out.println("[SGH-CLIENT]Percentage humidity:" + humidity + "%");
                    });
                });
            });
    }



    private void manageGraphicAppearence() {
        textArea.setBackground(Color.BLACK);
        textArea.setForeground(Color.YELLOW);
        this.buttonStart.setBackground(Color.YELLOW);
        buttonStart.setPreferredSize(new Dimension((int) (this.getWidth() * 0.8), (int) (this.getHeight() * 0.05)));
        scrollPanel.setPreferredSize(new Dimension((int) (this.getWidth() * 0.8), (int) (this.getHeight() * 0.8)));
        scrollPanel.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPanel.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPanel.getVerticalScrollBar().addAdjustmentListener(e-> {
                e.getAdjustable().setValue(e.getAdjustable().getMaximum());
        });
        this.textArea.setEnabled(false);
        textArea.setEditable(false);
        String[] porte = SerialPortList.getPortNames();
        for (int i = 0; i < porte.length; i++) {
                comboBox.addItem(porte[i]);
        }
        this.serialPortLable.setPreferredSize(new Dimension((int) (this.getWidth() * 0.3), (int) (this.getHeight() * 0.05)));
        comboBox.setPreferredSize(new Dimension((int) (this.getWidth() * 0.5), (int) (this.getHeight() * 0.05)));
        this.ngrokLabel.setPreferredSize(new Dimension((int) (this.getWidth() * 0.3), (int) (this.getHeight() * 0.05)));
        this.textNgrok.setPreferredSize(new Dimension((int) (this.getWidth() * 0.5), (int) (this.getHeight() * 0.05)));
        this.buttonStart.addActionListener(e-> {
            String[] args = new String[] {this.comboBox.getSelectedItem().toString(), "9600"};
            this.buttonStart.setEnabled(false);
            this.textArea.setEnabled(true);
            try {
                    check(args);
            } catch (Exception e1) {
                    e1.printStackTrace();
            }
        });
    }
    
    private void addElementToPanel() {
        this.add(scrollPanel);
        this.add(serialPortLable);
        this.add(comboBox);
        this.add(ngrokLabel);
        this.add(textNgrok);
        this.add(buttonStart);
    }
    
    public JTextArea getTextArea() {
        return this.textArea;
}
}