package gui;


import javax.swing.JFrame;

public class MyFrame extends JFrame {

        /**
         * 
         */
        private static final long serialVersionUID = 1L;
        private MyPanel panel ;
        
        public MyFrame(String string) {
                super(string);
                ScreenSize screenSize = new ScreenSize();
                this.setSize(screenSize.setDimension());
                panel = new MyPanel(this);
                this.getContentPane().add(panel);
                this.setVisible(true);
                this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                this.setLocationRelativeTo(null);
        }

        public MyPanel getPanel() {
                return this.panel;
        }
        
}
