package gui;

import java.awt.Dimension;
import java.awt.Toolkit;

public class Screen {
	
	public Screen() {
		
	}
	
	public Dimension setDimension() {
		Toolkit toolKit = Toolkit.getDefaultToolkit();
		Dimension screenDimension = toolKit.getScreenSize();
		screenDimension.setSize((int) 3 * screenDimension.getWidth() / 5, (int) screenDimension.getHeight() / 5);
		return screenDimension;
	}
}
