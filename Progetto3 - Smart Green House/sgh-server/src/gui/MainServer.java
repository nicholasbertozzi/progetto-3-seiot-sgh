package gui;

import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import common.DataService;
import io.vertx.core.Vertx;

public class MainServer {
	
	public static void main(String[] args) throws Exception {
		Vertx vertx = Vertx.vertx();
		DataService service = new DataService(8080);
		vertx.deployVerticle(service);
		
		
		
		JFrame frame = new JFrame("SGH - server");
		JLabel label = new JLabel("Connection to Ngrok service is running - ");
		JLabel label1 = new JLabel("Click the button to terminate the connection and terminate the connection");
		JButton exitButton = new JButton("Close connection");
		exitButton.addActionListener(action -> {
		    System.exit(0);
		});
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel panel = new JPanel(new FlowLayout());
		panel.add(label);
		panel.add(label1);
		panel.add(exitButton);
		frame.setContentPane(panel);
		frame.setSize(new Screen().setDimension());
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
	
}


