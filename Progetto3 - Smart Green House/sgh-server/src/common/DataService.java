package common;

import io.vertx.core.AbstractVerticle;
//import io.vertx.core.Vertx;
//import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpServerResponse;
//import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

//import java.util.ArrayList;
import java.util.Date;
//import java.util.HashMap;
//import java.util.LinkedList;
//import java.util.Map;

/*
 * Data Service as a vertx event-loop 
 */
public class DataService extends AbstractVerticle {

	private int port;
	//private static final int MAX_SIZE = 10;
	private int humidityPercentage;
	
	public DataService(int port) {
		this.humidityPercentage = 0;	
		this.port = port;
	}

	@Override
	public void start() {		
		Router router = Router.router(vertx);
		router.route().handler(BodyHandler.create());
		router.post("/api/data").handler(this::handleAddNewData);
		router.get("/api/data").handler(this::handleGetData);	
		vertx
			.createHttpServer()
			.requestHandler(router::accept)
			.listen(port);

		log("Service ready.");
	}
	
	private void handleAddNewData(RoutingContext routingContext) {
		HttpServerResponse response = routingContext.response();
		// log("new msg "+routingContext.getBodyAsString());
		JsonObject res = routingContext.getBodyAsJson();
		if (res == null) {
		    /* nel caso l'operazione per restituire non vada a buon fine, allora restituisce un valore di errore */
		    sendError(400, response);
		} else {
		    
		    this.humidityPercentage = res.getInteger("humidity");
		    /* questo metodo prende il valore di humidity dal file Json e me lo converte in valore intero */
		    long time = System.currentTimeMillis();
		    log("New value humudity: " + this.humidityPercentage + " in date: " + new Date(time));
		    /* quando il dato viene reperito correttamente, allora viene restituito il valore 200 che indica l'avvenuta operazione con successo */
		    response.setStatusCode(200).end();
		}
	}
	
	
	 //perch� ogni volta che arriva un nuovo valore lui effettua delle modifiche 
	private void handleGetData(RoutingContext routingContext) {
		JsonObject data = new JsonObject();
		data.put("humidityPercentage", humidityPercentage);
		routingContext.response()
			.putHeader("content-type", "application/json")
			.end(data.encodePrettily());
		//System.out.println("humidity percentage:" + this.humidityPercentage);
	}
	
	private void sendError(int statusCode, HttpServerResponse response) {
		response.setStatusCode(statusCode).end();
	}

	private void log(String msg) {
		System.out.println("[DATA SERVICE] "+msg);
	}

	/*public static void main(String[] args) {
		Vertx vertx = Vertx.vertx();
		DataService service = new DataService(8080);
		vertx.deployVerticle(service);
	}*/
}