#ifndef __SONAR__
#define __SONAR__
#include "VariousDefine.h"

class Sonar {
public:
	Sonar(int trigPin, int echoPin, int temp);
	Sonar(int trigPin, int echoPin);
	float getDistance();
private:
	int trigPin, echoPin;
	float soundVelocity;
};
#endif
