#include "Arduino.h"
#include "MsgServiceBT.h"


MsgServiceBT::MsgServiceBT(int rxPin, int txPin) {
	channel = new SoftwareSerial(txPin, rxPin);
}

void MsgServiceBT::init() {
	content.reserve(256);
	channel->begin(9600);
  channel->print("AT");
  channel->print("AT+BAUD4");
  channel->print("AT+NAME");
  channel->print("AT+NAMESmartGreenHouseBrt");
}

bool MsgServiceBT::sendMsg(MsgBT msg) {
	channel->println(msg.getContent());
}

bool MsgServiceBT::isMsgAvailable() {
	return channel->available();
}

MsgBT* MsgServiceBT::receiveMsg() {
	while (channel->available()) {
		char ch = (char)channel->read();
		if (ch == '\n') {
			MsgBT* msg = new MsgBT(content);
			content = "";
			return msg;
		}
		else {
			content += ch;
		}
	}
	return NULL;
}
