#ifndef __TASKDETECT__
#define __TASKDETECT__

#include "Task.h"
#include "Sonar.h"
#include "Led.h"
#include "MsgServiceBT.h"

extern int mood;

class TaskDetect : public Task {
public:
	TaskDetect(int trigPin, int echoPin, int l1Pin, int lmPin, int txPin, int rxPin);
	void init(int period, int basePeriod);
	void tick();
private:
	int echoPin, trigPin; /* pin sonar */
	int rxPin, txPin; /* pin modulo BT */
	int l1Pin, lmPin; /* pin led */
	int counter;
	float distance;
	Sonar* sonar;
	Led* l1;
	Led* lm;
	MsgServiceBT* msgServiceBT;
};
#endif
