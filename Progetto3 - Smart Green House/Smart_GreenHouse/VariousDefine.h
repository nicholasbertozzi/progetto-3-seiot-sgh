#define MAXTASKS 10



#define LMPIN 2 /* led rosso che indica la modalit� manuale */
#define L1PIN 4 /* led verde che indica la modalit� automatica */
#define L2PIN 5 /* led verde che indica l'apertura dellla pompa, PWM */
#define SERVOPIN 6 /* pin per il servo, PWM */
#define ECHOPIN 8
#define TRIGPIN 7
#define RXPIN 13
#define TXPIN 12 




#define AUTO 0 /* modalit� automatica del controller */
#define MANUAL 1 /* modalit� manuale del controllore */




#define DIST 0.3 /* distanza massima rilevata dal sonar per cui � possibile entrare in modalit� automatica */




/*COSTANTI PER L'IRRIGAZIONE*/
/*Portata acqua della pompa */
#define PMIN 50
#define PMED 100
#define PMAX 150
/*Massimo tempo di irrigazione*/
#define TMAX 5000 //timeout irrigazione in millisecondi
/*Valori umidità*/
#define UMIN 30
#define DELTAU 5
/*Step unidit�*/
#define HUMIDITYMIN 10
#define HUMIDITYMED 20
#define HUMIDITYMAX 30
/*valori intensit� per la scrittura pwm del led*/
#define INTENISTYMIN 30
#define INTENISTYMED 100
#define INTENISTYMAX 255
/*valori per i vari step di apertura del servo*/
#define SERVOCLOSE 750
#define SERVOMIN 1250
#define SERVOMED 1750
#define SERVOMAX 2250





/*TEMPI SCHEDULER*/
#define BASEPERIOD 500
#define TASKIRRIGATEPERIOD 1000
#define TASKDETECTPERIOD 500
