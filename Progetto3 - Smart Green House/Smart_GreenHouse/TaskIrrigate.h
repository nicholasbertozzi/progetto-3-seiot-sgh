#ifndef __TASKIRRIGATE__
#define __TASKIRRIGATE__

#include "Task.h"
#include "ServoMotor.h"
#include "Led.h"
#include "MsgService.h"
#include "MsgServiceBT.h"
#include "VariousDefine.h"

extern int mood;

class TaskIrrigate:public Task {
public:
	TaskIrrigate(int rxPin, int txPin, int servoPin, int l2Pin);
	void init(int period, int basePeriod);
	void tick();
private:
	int rxPin, txPin; /*pin BT*/
	int servoPin; /*pin servo*/
	int l2Pin; /*pin led intensit�*/
	int humidity, flow;
	bool irrigationInActionAuto, irrigationInActionManual;
	long timeIrrigationToReach;

	Led* l2;
	ServoMotor* servoMotor;
	MsgServiceBT* msgServiceBT;
};
#endif
