#ifndef __TASK__
#define __TASK__
#include "VariousDefine.h"
#include "Arduino.h"

extern int state;

class Task {
private:
	int period, basePeriod, timeEllapsed;
public:
	/*	RICORDA:
	period: periodo con la frequenza con cui va eseguito il task
	basePeriod: perido con cui gira lo scheduler
	*/
	virtual void init(int period, int basePeriod) {
		this->period = period;
		this->basePeriod = basePeriod;
		timeEllapsed = 0;
	}

	bool updateAndCheckTime() {
		timeEllapsed += basePeriod;
		if (timeEllapsed >= period) {
			timeEllapsed = 0;
			return true;
		}
		else {
			return false;
		}
	}
	virtual void tick() = 0;
	int getPeriod() {
		return period;
	}


};
#endif
