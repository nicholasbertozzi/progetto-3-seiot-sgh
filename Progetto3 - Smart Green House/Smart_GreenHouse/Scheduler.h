#ifndef __SCHEDULER__
#define __SCHEDULER__
#include "Task.h"
#include "Timer.h"
#include "VariousDefine.h" 

class Scheduler {
public:
	Scheduler();
	void init(int basePeriod);
	bool addTask(Task* task);
	void schedul();
private:
	int basePeriod, taskNum;
	Timer* timer;
	Task* taskList[MAXTASKS];

};
#endif
