#include "TaskDetect.h"
#include "VariousDefine.h"

TaskDetect::TaskDetect(int trigPin, int echoPin, int l1Pin, int lmPin, int txPin, int rxPin) {
	this->echoPin = echoPin;
	this->trigPin = trigPin;
	this->l1Pin = l1Pin;
	this->lmPin = lmPin;
	this->rxPin = rxPin;
	this->txPin = txPin;
	counter = 0;
	distance = 0;
	sonar = new Sonar(trigPin, echoPin);
	l1 = new Led(l1Pin);
	lm = new Led(lmPin);
	msgServiceBT = new MsgServiceBT(rxPin, txPin);
	msgServiceBT->init();
	mood = AUTO;
}

void TaskDetect::init(int period, int basePeriod) {
	Task::init(period, basePeriod);
}

void TaskDetect::tick() {
	counter++;
	distance = sonar->getDistance();
	//Serial.println("Distance: " + String(distance));



	if (distance > DIST) { /* il controller agir� in modalit� automatica */
		if (mood != AUTO) {
			mood = AUTO;
			Serial.println("Transition to automatic... System: AUTO");
			msgServiceBT->sendMsg(MsgBT("Transition to automatic... System: AUTO"));
			lm->switchOff();
			l1->switchOn();
		}
	}
	else { /* il controller agir� in modalit� manuale con le direttive impartite tramite bt dall'app android */
		if (mood != MANUAL) {
			mood = MANUAL;
			Serial.println("Transition to manual... System: MANUAL");
			msgServiceBT->sendMsg(MsgBT("Transition to manual... System: MANUAL"));
			l1->switchOff();
			lm->switchOn();
		}
	}
}
