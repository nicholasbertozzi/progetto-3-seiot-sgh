#ifndef __SERVOMOTOR__
#define __SERVOMOTOR__

#include "Servo.h"
#include "ServoTimer2.h"

class ServoMotor : public Servo {
public:
	ServoMotor(int servoPin);
	void turnOn();
	void turnOff();
	void setPosition(int position);
private:
	int servoPin;
	ServoTimer2 servoMotorTimer2;
};
#endif
