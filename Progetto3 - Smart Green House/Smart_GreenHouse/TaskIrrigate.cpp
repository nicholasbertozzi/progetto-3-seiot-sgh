#include "TaskIrrigate.h"
#include <string.h>

TaskIrrigate::TaskIrrigate(int rxPin, int txPin, int servoPin, int l2Pin) {
	this->rxPin = rxPin;
	this->txPin = txPin;
	this->servoPin = servoPin;
	this->l2Pin = l2Pin;
	l2 = new Led(l2Pin);
	servoMotor = new ServoMotor(servoPin);
	msgServiceBT = new MsgServiceBT(rxPin, txPin);
  msgServiceBT->init();
	irrigationInActionAuto = false;
	irrigationInActionManual = false;
	flow = 0;
	humidity = -1;
	randomSeed(analogRead(A0));
}

void TaskIrrigate::init(int period, int basePeriod) {
	Task::init(period, basePeriod);
}

void TaskIrrigate::tick() {
 if (MsgService.isMsgAvailable()) {//*controllo se presente un valore dell'unidita
    Msg* msg = MsgService.receiveMsg();
    humidity = msg->getContent().toInt();
    servoMotor->turnOn();
  }
  //servoMotor->turnOn();
  //humidity = random(50);
  Serial.println("Humidity percentage: " + String(humidity) + "%");

  if (humidity != -1) {
    if (mood == AUTO) { /*controlli sull'irrigazione effettuati in modo automatico*/
       msgServiceBT->sendMsg(MsgBT("exit"));
       //Serial.println("Send to mobile, close connection\n");
      if (irrigationInActionManual == true) {
        irrigationInActionManual = false;
        Serial.println("Water pump closed, for switching to automatic mode");
        l2->switchOff();
        servoMotor->setPosition(SERVOCLOSE);
      }

      if (irrigationInActionAuto == false) {
        /*UMIDITA' MINIMA*/
        if (humidity <= HUMIDITYMIN) {
          flow = PMAX;
          irrigationInActionAuto = true;
          Serial.println("Water pump open");
          Serial.println("Water flow high: " + String(flow) + " liters/minute");
          l2->switchOnPWM(INTENISTYMAX);
          servoMotor->setPosition(SERVOMAX);
          timeIrrigationToReach = millis() + TMAX;
        }
        /*UMIDITA' MEDIA*/
        if (humidity > HUMIDITYMIN && humidity <= HUMIDITYMED) {
          flow = PMED;
          irrigationInActionAuto = true;
          Serial.println("Water pump open");
          Serial.println("Water flow medium: " + String(flow) + " liters/minute");
          l2->switchOnPWM(INTENISTYMED);
          servoMotor->setPosition(SERVOMED);
          timeIrrigationToReach = millis() + TMAX;
        }
        /*UMIDITA' MASSIMA*/
        if (humidity > HUMIDITYMED && humidity <= HUMIDITYMAX) {
          flow = PMIN;
          irrigationInActionAuto = true;
          Serial.println("Water pump open");
          Serial.println("Water flow low: " + String(flow) + " liters/minute");
          l2->switchOnPWM(INTENISTYMIN);
          servoMotor->setPosition(SERVOMIN);
          timeIrrigationToReach = millis() + TMAX;
        }
      }

      if (irrigationInActionAuto == true && millis() >= timeIrrigationToReach) {
        irrigationInActionAuto = false;
        Serial.println("Water pump close, timeout irrigation reached");
        l2->switchOff();
        servoMotor->setPosition(SERVOCLOSE);
      }

      if (irrigationInActionAuto == true && humidity >= UMIN + DELTAU) {
        irrigationInActionAuto = false;
        Serial.println("Water pump close, humidity level reached");
        l2->switchOff();
        servoMotor->setPosition(SERVOCLOSE);
      }
    }

    /* COLLEGAMENTO CON ARDUINO IN MODALITA' MAUALE BLUETOOTH */
    if(mood==MANUAL) {
      if (irrigationInActionAuto == true) {
        irrigationInActionAuto = false;
        Serial.println("Water pump closed, for switching to manual mode"); /*da inviare con BT??*/
        l2->switchOff();
        servoMotor->setPosition(SERVOCLOSE);
      }
      msgServiceBT->sendMsg(MsgBT(String(humidity)));

      if (msgServiceBT->isMsgAvailable()) {
        MsgBT* msgBT = msgServiceBT->receiveMsg();
        String message = msgBT->getContent();
        Serial.println("recived bluetooth: " + message);

        /*PORTATA ACQUA AL MINIMO*/
        if (message == "min") {
          flow = PMIN;
          irrigationInActionManual = true;
          Serial.println("Water pump open,manual command");
          Serial.println("Water flow low: " + String(flow) + " liters/minute"); /*da inviare con BT??*/
          l2->switchOnPWM(INTENISTYMIN);
          servoMotor->setPosition(SERVOMIN);
          //timeIrrigationToReach = millis() + TMAX;
        }
        /*PORTATA ACQUA MEDIA*/
        if (message == "med") {
          flow = PMED;
          irrigationInActionManual = true;
          Serial.println("Water pump open, manual command");
          Serial.println("Water flow medium: " + String(flow) + " liters/minute");/*da inviare con BT??*/ 
          l2->switchOnPWM(INTENISTYMED);
          servoMotor->setPosition(SERVOMED);
          //timeIrrigationToReach = millis() + TMAX;
        }
        /*PORTATA ACQUA MASSIMA*/
        if (message == "max") {
          flow = PMAX;
          irrigationInActionManual = true;
          Serial.println("Water pump open, manual command");
          Serial.println("Water flow high: " + String(flow) + " liters/minute"); /*da inviare con BT??*/
          l2->switchOnPWM(INTENISTYMAX);
          servoMotor->setPosition(SERVOMAX);
          //timeIrrigationToReach = millis() + TMAX;
        }
        /*CHIUSURA POMPA*/
        if (message == "close") {
          irrigationInActionManual = false;
          Serial.println("Water pump close, manual command");
          l2->switchOff();
          servoMotor->setPosition(SERVOCLOSE);
        } 
      }
    }
      /*NON SONO SICURO VADANO FATTI QUESTI CONTROLLI IN MODALITA' MANUALE*/
      /*if (irrigationInActionManual == true && millis() >= timeIrrigationToReach) {
        irrigationInActionManual = false;
        Serial.println("Water pump close, timeout irrigation reached"); //da inviare con BT
        l2->switchOff();
        servoMotor->setPosition(SERVOCLOSE);
      }

      if (irrigationInActionManual == true && humidity >= UMIN + DELTAU) {
        irrigationInActionManual = false;
        Serial.println("Water pump close, humidity level reached"); //da inviare con BT
        l2->switchOff();
        servoMotor->setPosition(SERVOCLOSE);
      }*/
    }  else {
    Serial.println("No humidity value, control connection to sensor");
  }
}
