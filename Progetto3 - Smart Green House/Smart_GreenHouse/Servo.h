#ifndef __SERVO__
#define __SERVO__

class Servo {
public:
	virtual void turnOn() = 0;
	virtual void turnOff() = 0;
	virtual void setPosition(int position) = 0;
};
#endif
