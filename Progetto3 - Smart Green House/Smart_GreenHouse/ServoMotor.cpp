#include "ServoMotor.h"

ServoMotor::ServoMotor(int servoPin) {
	this->servoPin = servoPin;
}

void ServoMotor::turnOn() {
	servoMotorTimer2.attach(servoPin);
}

void ServoMotor::turnOff() {
	servoMotorTimer2.detach();
}

void ServoMotor::setPosition(int position) {
	servoMotorTimer2.write(position);
}
