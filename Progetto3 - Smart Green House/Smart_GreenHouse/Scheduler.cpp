#include "Scheduler.h"

Scheduler::Scheduler() {}

void Scheduler::init(int basePeriod) {
	this->basePeriod = basePeriod;
	timer = new Timer();
	timer->setupPeriod(basePeriod);
	taskNum = 0;
	Serial.begin(9600);
}

bool Scheduler::addTask(Task* task) {
	if (taskNum < MAXTASKS - 1) {
		taskList[taskNum] = task;
		taskNum++;
		return true;
	}
	else
		return false;
}

void Scheduler::schedul() {
	timer->waitForNextTick();
	for (int c = 0; c < taskNum; c++) {
		if (taskList[c]->updateAndCheckTime()) {
			taskList[c]->tick();
		}
	}
}
