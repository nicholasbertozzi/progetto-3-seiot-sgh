// Visual Micro is in vMicro>General>Tutorial Mode
// 
/*
    Name:       Smart_GreenHouse.ino
    Created:	17/02/2019 21:15:34
    Author:     PICCIO-PC\Nicholas
*/

#include "Scheduler.h"
#include "TaskIrrigate.h"
#include "TaskDetect.h"
#include "VariousDefine.h"

extern int mood;
Scheduler scheduler;
Led* l1;

// The setup() function runs once each time the micro-controller starts
void setup()
{
	scheduler.init(BASEPERIOD);
	/*Inizializzazione task detect*/
	Task* taskDetect = new TaskDetect(TRIGPIN, ECHOPIN, L1PIN, LMPIN, TXPIN, RXPIN);
	taskDetect->init(TASKDETECTPERIOD, BASEPERIOD);
	/*Inizializzazione task irrigate*/
	Task* taskIrrigate = new TaskIrrigate(RXPIN, TXPIN, SERVOPIN, L2PIN);
	taskIrrigate->init(TASKIRRIGATEPERIOD, BASEPERIOD);
	/*Aggiunta dei task allo scheduelr*/
	scheduler.addTask(taskDetect);
	scheduler.addTask(taskIrrigate);

	mood = AUTO;
	l1 = new Led(L1PIN);
	l1->switchOn();
}

// Add the main program code into the continuous loop() function
void loop()
{
	scheduler.schedul();
}
