#include "Arduino.h"
#include "Sonar.h"

Sonar::Sonar(int trigPin, int echoPin, int temp) {
	this->trigPin = trigPin;
	this->echoPin = echoPin;
	pinMode(trigPin, OUTPUT);
	pinMode(echoPin, INPUT);
	this->soundVelocity = 331.45 + 0.62 * temp;
}

Sonar::Sonar(int trigPin, int echoPin) {
	this->trigPin = trigPin;
	this->echoPin = echoPin;
	pinMode(trigPin, OUTPUT);
	pinMode(echoPin, INPUT);
	this->soundVelocity = 331.45 + 0.62 * 20;
}

float Sonar::getDistance() {
	digitalWrite(trigPin, LOW);
	delayMicroseconds(3);
	digitalWrite(trigPin, HIGH);
	delayMicroseconds(5);
	digitalWrite(trigPin, LOW);

	float totalTravelTime = pulseIn(echoPin, HIGH); /* tempo in microsecondi, compreso di andata e ritorno */
	return (soundVelocity * (totalTravelTime / 1000000.0 / 2)); /* ritorna la lunghezza in metri */
}
