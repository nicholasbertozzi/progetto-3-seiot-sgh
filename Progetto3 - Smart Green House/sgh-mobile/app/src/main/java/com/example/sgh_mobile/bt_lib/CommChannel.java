package com.example.sgh_mobile.bt_lib;

public interface CommChannel {
    void close();

    void registerListener(Listener listener);

    void removeListener(Listener listener);

    String getRemoteDeviceName();

    void sendMessage(String message);

    interface Listener {
        void onMessageReceived(String receivedMessage);
        void onMessageSent(String sentMessage);
    }
}
