package com.example.sgh_mobile.utils;

public class C {

    public static final String APP_LOG_TAG = "BT CLN";

    public class bluetooth {
        public static final int ENABLE_BT_REQUEST = 1;
        public static final String BT_DEVICE_ACTING_AS_SERVER_NAME = "SmartGreenHouseBrt"; //MODIFICARE QUESTA COSTANTE CON IL NOME DEL DEVICE CHE FUNGE DA SERVER
        public static final String BT_SERVER_UUID = "00001101-0000-1000-8000-00805F9B34FB";
    }
}
