package com.example.sgh_mobile;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.sgh_mobile.bt_lib.BluetoothChannel;
import com.example.sgh_mobile.bt_lib.BluetoothUtils;
import com.example.sgh_mobile.bt_lib.ConnectToBluetoothServerTask;
import com.example.sgh_mobile.bt_lib.ConnectionTask;
import com.example.sgh_mobile.bt_lib.RealBluetoothChannel;
import com.example.sgh_mobile.bt_lib.exceptions.BluetoothDeviceNotFound;
import com.example.sgh_mobile.utils.C;

import java.util.UUID;

public class MainActivity extends AppCompatActivity {
    public static final int MINFLOW = 50;
    public static final int MEDIUMFLOW = 100;
    public static final int MAXFLOW = 150;

    private Button openPump;
    private Button closePump;
    private Button startCom;
    private RadioButton liters50;
    private RadioButton liters100;
    private RadioButton liters150;
    private TextView textView;

    private boolean btConnection;
    private boolean connectionTSGHController;
    private int flowRate;

    //private BluetoothAdapter btAdapter;
    private BluetoothChannel btChannel;


    @RequiresApi(api = Build.VERSION_CODES.ECLAIR)
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //pumpIsOpen = false;
        connectionTSGHController = false;
        flowRate = 0;

        openPump = findViewById(R.id.openPump);
        /*colore*/openPump.setBackgroundColor(Color.GREEN);
        /*disabilitato*/openPump.setEnabled(false);
        closePump = findViewById(R.id.closePump);
        /*colore*/closePump.setBackgroundColor(Color.RED);
        /*disabilitato*/closePump.setEnabled(false);
        startCom = findViewById(R.id.comunicationButton);
        /*colore*/startCom.setBackgroundColor(Color.YELLOW);
        liters50 = findViewById(R.id.liters50);
        /*disabilitato*/liters50.setEnabled(false);
        liters100 = findViewById(R.id.liters100);
        /*disabilitato*/liters100.setEnabled(false);
        liters150 = findViewById(R.id.liters150);
        /*disabilitato*/liters150.setEnabled(false);
        textView = findViewById(R.id.textView);
        /*movimento verticale*/textView.setMovementMethod(new ScrollingMovementMethod());

        final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();

        if(btAdapter != null && !btAdapter.isEnabled()){
            startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), C.bluetooth.ENABLE_BT_REQUEST);
        }

        initComponent();
    }

    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    void initComponent() {
        comunicateButtonListener();
        openPumpListener();
        closePumpListener();
        liters50RadioListener();
        liters100RadioListener();
        liters150RadioListener();
    }

    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    void comunicateButtonListener() {
        startCom.setOnClickListener(listener -> {
            if(!connectionTSGHController) {
                try {
                    textView.append("Attempt to connect to sgh-controller...\n");
                    connectToBTSGHClient();
                } catch (BluetoothDeviceNotFound bluetoothDeviceNotFound) {
                    bluetoothDeviceNotFound.printStackTrace();
                }
            } else {
                connectionTSGHController = false;
                startCom.setText("Start comunication\n");
                startCom.setBackgroundColor(Color.YELLOW);
                btChannel.close();
                restoreInitialInterface();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    private void connectToBTSGHClient() throws BluetoothDeviceNotFound {
        final BluetoothDevice serverDevice = BluetoothUtils.getPairedDeviceByName(C.bluetooth.BT_DEVICE_ACTING_AS_SERVER_NAME);
        final UUID uuid = BluetoothUtils.generateUuidFromString(C.bluetooth.BT_SERVER_UUID);

        AsyncTask<Void, Void, Integer> execute = new ConnectToBluetoothServerTask(serverDevice, uuid, new ConnectionTask.EventListener() {
            @Override
            public void onConnectionActive(final BluetoothChannel channel) {

                textView.append("Connection to sgh-controller enstablished\n");
                connectionTSGHController = true;
                btConnection = true;
                openPump.setEnabled(true);
                startCom.setText("Terminate comunication\n");
                startCom.setBackgroundColor(Color.RED);

                btChannel = channel;
                btChannel.registerListener(new RealBluetoothChannel.Listener() {
                    @Override
                    public void onMessageReceived(String receivedMessage) {
                        if(receivedMessage.equals("exit")) {
                            textView.append("Connection to sgh-controller lost, distance greater than 30 centimeters\n");
                            btChannel.close();
                            restoreInitialInterface();
                        } else {
                            textView.append(String.format("> [RECEIVED from %s] %s\n", btChannel.getRemoteDeviceName(), receivedMessage));
                        }
                    }

                    @Override
                    public void onMessageSent(String sentMessage) {
                        textView.append(String.format("> [SENT to %s] %s\n", btChannel.getRemoteDeviceName(), sentMessage));
                    }
                });
            }

            @Override
            public void onConnectionCanceled() {
                textView.append("sgh-controller not found!\n");
                btConnection = false;
            }
        });
        execute.execute();
    }

    void restoreInitialInterface() {
        connectionTSGHController = false;
        //pumpIsOpen = false;
        startCom.setEnabled(true);
        startCom.setText("Start Comunication");
        startCom.setBackgroundColor(Color.YELLOW);
        openPump.setEnabled(false);
        closePump.setEnabled(false);
        liters50.setEnabled(false);
        liters50.setChecked(false);
        liters100.setEnabled(false);
        liters100.setChecked(false);
        liters150.setEnabled(false);
        liters150.setChecked(false);
    }

    void openPumpListener() {
        openPump.setOnClickListener(listener -> {
            openPump.setEnabled(false);
            closePump.setEnabled(true);
            liters50.setEnabled(true);
            liters50.setChecked(true);
            liters100.setEnabled(true);
            liters150.setEnabled(true);
            //pumpIsOpen = true;
            flowRate = MINFLOW;
            btChannel.sendMessage("min");
            textView.append("Water pump open...\n");
            textView.append("water flow rate: " + MINFLOW + "liters/minute\n");
        });
    }

    void closePumpListener() {
        closePump.setOnClickListener(listener -> {
            openPump.setEnabled(true);
            closePump.setEnabled(false);
            liters50.setChecked(false);
            liters100.setChecked(false);
            liters150.setChecked(false);
            liters50.setEnabled(false);
            liters100.setEnabled(false);
            liters150.setEnabled(false);
            //pumpIsOpen = false;
            flowRate = 0;
            btChannel.sendMessage("close");
            textView.append("Water pump close...\n");
        });
    }

    void liters50RadioListener() {
        liters50.setOnClickListener(listener -> {
            if(flowRate != MINFLOW) {
                liters50.setChecked(true);
                liters100.setChecked(false);
                liters150.setChecked(false);
                flowRate = MINFLOW;
                btChannel.sendMessage("min");
                textView.append("water flow rate: " + MINFLOW + "liters/minute\n");
            }
        });
    }

    void liters100RadioListener() {
        liters100.setOnClickListener(listener -> {
            if(flowRate != MEDIUMFLOW) {
                liters100.setChecked(true);
                liters50.setChecked(false);
                liters150.setChecked(false);
                flowRate = MEDIUMFLOW;
                btChannel.sendMessage("med");
                textView.append("water flow rate: " + MEDIUMFLOW + "liters/minute\n");
            }
        });

    }

    void liters150RadioListener() {
        liters150.setOnClickListener(listener -> {
            if(flowRate != MAXFLOW) {
                liters150.setChecked(true);
                liters50.setChecked(false);
                liters100.setChecked(false);
                flowRate = MAXFLOW;
                btChannel.sendMessage("max");
                textView.append("water flow rate: " + MAXFLOW + "liters/minute\n");
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        if(btConnection) {
            btChannel.close();
        }
        restoreInitialInterface();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(btConnection) {
            btChannel.close();
        }
        restoreInitialInterface();
    }



    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, @Nullable final Intent data) {
        if(requestCode == C.bluetooth.ENABLE_BT_REQUEST && resultCode == RESULT_OK){
            Log.d(C.APP_LOG_TAG, "Bluetooth enabled!");
        }

        if(requestCode == C.bluetooth.ENABLE_BT_REQUEST && resultCode == RESULT_CANCELED){
            Log.d(C.APP_LOG_TAG, "Bluetooth not enabled!");
        }
    }
}
